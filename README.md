# Idris2-toolbox

[![pipeline status](https://gitlab.com/niklashh/idris2-toolbox/badges/master/pipeline.svg)](https://gitlab.com/niklashh/idris2-toolbox/-/commits/master)

This container can be used for running idris2 inside [toolbox](https://github.com/containers/toolbox).

```console
toolbox create --image registry.gitlab.com/niklashh/idris2-toolbox:latest
toolbox enter idris2-toolbox
```

## Building manually

To build the toolbox container

```console
./build.sh
```

To start the toolbox

```console
toolbox create --image localhost/idris2-toolbox
toolbox enter idris2-toolbox
```

To remove the toolbox

```console
toolbox rm -f idris2-toolbox
```

## License

MIT
