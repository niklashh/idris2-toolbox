ARG IDRIS_VERSION=latest

FROM ghcr.io/joshuanianji/idris-2-docker/base:${IDRIS_VERSION} as base

FROM registry.gitlab.com/niklashh/debian-toolbox:latest

LABEL com.github.containers.toolbox="true" \
      usage="This image is meant to be used with the toolbox command" \
      maintainer="Niklas Halonen <niklas.2.halonen@aalto.fi>"

ENV PATH="/root/.idris2/bin:${PATH}"

# add idris2 and scheme from builder
COPY --from=base /root/.idris2 /root/.idris2
COPY --from=base /usr/bin/scheme /usr/bin/scheme
# copy csv9.5* to /usr/lib
COPY --from=base /root/scheme-lib/ /usr/lib/

RUN apt-get update \
 && apt-get -y install rlwrap \
 && apt-get clean \
 && chown -R 1000:1000 /root \
 && idris2 --version